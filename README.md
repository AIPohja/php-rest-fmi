# README #

This script was created for a Thingworx ICT project at HAMK University of Applied Sciences. The idea was to mimic an IOT weather station and to utilize the Thingworx REST API.

### What does the script do? ###

The script gets data from Finnish Meteorological Institute's open data services. Some data is extracted from the FMI XML data and then converted to JSON. Finally the data is sent to a Thingworx Thing.


### How do I get set up? ###

To run the script you need a server with PHP 5 or later installed

You also need  

 
* An API key from the Finnish Meteorological Institute  
* A Thingworx environment and a Thing  
* A Thingworx app key that has access to the Thing  

You also need to set up a cron timer to run the script for example every 10 minutes. 

SSH to the server and edit your crontab with the command `crontab -e`. Then enter the timings you want. 

This example calls php program to run the script every 10 minutes:
```
#!cron
5,15,25,35,45,55 * * * * php path/to/rest.php
```