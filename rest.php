<?php

// initialize necessary variables, replace with your own values

$place = "hämeenlinna";
$fmikey = "insert-your-fmi-key-here"; // Finnish Meteorological Institute's API key
$path = "http://data.fmi.fi/fmi-apikey/". $fmikey ."/wfs?request=getFeature&storedquery_id=fmi::observations::weather::multipointcoverage&place=" . $place;
$appkey = "your-thingworx-api-key-here";
$thingpath = "https://path.to.thingworx/Thingworx/Things/your-thing-name/Properties/";
$twx_path = $thingpath . "*?appKey=" . $appkey . "&x-thingworx-session=true";

// functions begin
function download_page($url_path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url_path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    $retValue = curl_exec($ch);
    curl_close($ch);
    return $retValue;
}

// a PUT call to a Thingworx thing to send data
function send_data_to_thing($url_path, $appkey, $temp, $wind, $loc, $pos_lat, $pos_long, $date, $time) {
    $service_url = $url_path;
    /* 
        Thingworx LOCATION datatype consists of latitude, longitude, 
        elevation and units (coordinate system) so it is put into an array
    */ 
    $position = array(
        "latitude"=>$pos_lat,
        "longitude"=>$pos_long,
        "elevation"=>0,
        "units"=>"WGS84"
        ); 
    
    // all data that needs to be sent is put into an array, which is then decoded to Json
    $data = array(
        "temperature" => $temp,
        "wind" => $wind,
        "location" => strval($loc),
        "position" => $position, 
        "date" => $date,
        "time" => $time
        );
    $data_json = json_encode($data);    

    // PHP curl to send the data
    $ch = curl_init($service_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // set HTTP header content type and length
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Content-Length:' . strlen($data_json), 'Accept:application/json'));    
    // set HTTP method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    // add the JSON data to be sent
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);    
    $response = curl_exec($ch);
          
    
    // if response fails, print the result
    if ($response === false) {
        $info = curl_getinfo($ch);
        
        curl_close($ch);
        die('error occured during curl exec. Additional info: ' . $info);
    }
    /*
    // uncomment for printing
    echo $data_json;
    echo "<p>" . $response . "</p>"; 
       echo "<p>" . $info . "</p>";
    */  
    curl_close($ch);
}


// functions END

// initialize the XML object
$sXML = download_page($path);
$oXML = new SimpleXMLElement($sXML);

// get the XML namespaces from document and  register them for later use
foreach($oXML->getDocNamespaces() as $strPrefix => $strNamespace) {
    if(strlen($strPrefix) == 0) {
        $strPrefix="a";
    }
    $oXML->registerXPathNamespace($strPrefix, $strNamespace);
}

// get location, position and measurement data from the xml
$location = $oXML->xpath("//gml:name")[0];
$position = $oXML->xpath("//gml:pos")[0];
$data = $oXML->xpath("//gml:doubleOrNilReasonTupleList")[0];

//get time and make it human readable
$pos_time = $oXML->xpath("//gmlcov:positions")[0];
$pos_time_arr = explode("\n", $pos_time);
array_pop($pos_time_arr);
$pos_time_most_recent = end(array_values($pos_time_arr));
$date = date('d.m.Y', substr($pos_time_most_recent, -10));
$time = date('H:i', substr($pos_time_most_recent, -10));

/*
// uncomment for printing
foreach ($data_arr as $line) {
echo $line . "<br>";
}
*/

//explode the $data string to an array using linebreak "/n" as brakpoint
$data_arr = explode("\n", $data);
array_pop($data_arr);

/*
// uncomment for printing
echo "Location: " . $location . "<br>";
echo "Position: " . $position . "<br>";
echo "Date and time: " . $date . " " .$time . "<br>";
*/

//get the most recent weather data row from $data_arr
$mostrecent = end(array_values($data_arr));
$obs_array = explode(" ", trim($mostrecent));

/*
// uncomment for printing
echo "Temperature: " . $obs_array[0] . " °C<br>";
echo "Wind speed: " . $obs_array[1] . " m/s<br>";
echo "Gust speed: " . $obs_array[2] . " m/s<br>";
echo "Wind direction: " . $obs_array[3] . " degrees<br>";
echo "Relative air humidity: " . $obs_array[4] . " %<br>";
echo "Dew point temperature: " . $obs_array[5] . " °C<br>";
echo "Precipitation amount: " . $obs_array[6] . " mm<br>";
echo "Precipitation intensity: " . $obs_array[7] . " mm/h<br>";
echo "Snow depth: " . $obs_array[8] . " cm<br>";
echo "Air pressure: " . $obs_array[9] . " hPa<br>";
echo "Visibility: " . $obs_array[10] . " m<br>";
echo "Cloud amount: " . $obs_array[11] . " oktas (1/8)<br>";
echo "<br/><br/>";
*/

// get latitude and longitude
$pos_lat = explode(" ", $position)[0];
$pos_long = explode(" ", $position)[1];

// send all the required variables to the send_data_to_thing() function
send_data_to_thing($twx_path, $appkey, $obs_array[0], $obs_array[1], $location, $pos_lat, $pos_long, $date, $time);
?>